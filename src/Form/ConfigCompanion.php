<?php

namespace Drupal\bible_companion\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;

/**
 * Configure Bible Companion.
 */
class ConfigCompanion extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bible_companion.admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['bible_companion.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bible_companion.settings');

    $form['bible_verse'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bible verse url'),
      '#default_value' => ($config->get('bible_verse') == '') ?
        'https://dailyverses.net/getdailyverse.ashx?language=en&date='
        : $config->get('bible_verse'),
    ];

    $form['daily_exhortation_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Daily exhortation url'),
      '#default_value' => ($config->get('daily_exhortation_url') == '') ?
        'https://www.christadelphian.or.tz/exhort_service/'
        : $config->get('daily_exhortation_url'),
    ];

    $form['daily_exhortation_more'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Daily exhortation more'),
      '#default_value' => ($config->get('daily_exhortation_more') == '') ?
        '<p><a target="_blank" href="https://www.christadelphian.or.tz/exhort_service/all">More</a></p>'
        : $config->get('daily_exhortation_more'),
    ];


    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('bible_companion.settings');
    $bibleVerse = $form_state->getValue('bible_verse');
    $config
      ->set('bible_verse', $bibleVerse)
      ->save();

    $dailyExhort = $form_state->getValue('daily_exhortation_url');
    $config
      ->set('daily_exhortation_url', $dailyExhort)
      ->save();

    $dailyExhortMore = $form_state->getValue('daily_exhortation_more');
    $config
      ->set('daily_exhortation_more', $dailyExhortMore)
      ->save();

    Cache::invalidateTags(['bible_companion']);
    parent::submitForm($form, $form_state);
  }

}
