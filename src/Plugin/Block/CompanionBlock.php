<?php

namespace Drupal\bible_companion\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Component\Serialization\Yaml;

/**
 * ProvidesBible Companion Block.
 *
 * @Block(
 *   id = "bible_companion_block",
 *   admin_label = @Translation("Bible Companion block"),
 *   category = @Translation("Bible Companion block"),
 * )
 */
class CompanionBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $path = DRUPAL_ROOT  . '/' . drupal_get_path('module', 'bible_companion');
    $fileContents = file_get_contents($path . '/readings.yml');
    $readings = Yaml::decode($fileContents);

    $month = date('M', time());
    $day = date('d', time());

    $entry = $readings[$month][$day];

    $renderable = [
      '#theme' => 'bible_companion',
      '#readings' => $entry,
    ];
    $blockContent = \Drupal::service('renderer')->renderPlain($renderable);

    return [
      '#markup' => $blockContent,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}
