<?php

namespace Drupal\bible_companion\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides Rondom Verse Block.
 *
 * @Block(
 *   id = "random_verse_block",
 *   admin_label = @Translation("Random Verse block"),
 *   category = @Translation("Random Verse block"),
 * )
 */
class RandomVerseBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $blockContent = "<strong>RANDOM BIBLE VERSE</strong></br>";
    $year = date('Y');
    $month = date('m');
    $day = date('m');
    $url = 'https://dailyverses.net/getdailyverse.ashx?language=en&date=' .
      $year . '/' . $month . '/' . $day . '/kjv';
    $page = '<span class="dailyverses">' . file_get_contents($url) . '</span>';
    $blockContent .= $page;
    $blockContent .= '<a href="http://dailyverses.net/">DailyVerses.net</a>';

    return [
      '#markup' => $blockContent,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}
