<?php

namespace Drupal\bible_companion\Plugin\Block;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Block\BlockBase;

/**
 * Provides Daily Exhortation Block.
 *
 * @Block(
 *   id = "daily_exhortation_block",
 *   admin_label = @Translation("Daily Exhortation block"),
 *   category = @Translation("Daily Exhortation block"),
 * )
 */
class DailyExhortationBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $blockContent = "<strong>DAILY EXHORTATION</strong></br>";
    $month = date('n');
    $day = date('d');
    $site = \Drupal::config('bible_companion.settings')
      ->get('daily_exhortation_url');
    $url = $site . $month . "/" . $day;


    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $page = curl_exec($ch);
    curl_close($ch);


    $body = Xss::filterAdmin(substr($page, 0, 500));
    $more = \Drupal::config('bible_companion.settings')
      ->get('daily_exhortation_more');

    $blockContent .= $body . $more;

    return [
      '#markup' => $blockContent,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}
